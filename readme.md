# vk-blackjack-bot

## Project setup

- `git clone` this repo
- `cp config.json.example config.json` and edit the latter
- You may want to use `venv`, in that case:
- - `python3 -m venv venv` to create virtual environment
- - `source venv/bin/activate` to activate it (`.\venv\bin\activate.bat` for windows)
- `python3 -m pip install -r requirements.txt` to install dependencies

## Project launch

- `python3 app.py` to start the loop
- You may want to append ` > log.txt` to redirect output
- - If you use `venv`, activate it before launch
