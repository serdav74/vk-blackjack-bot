import logging
from datetime import datetime
from random import randint
from blackjack import BlackjackGame, CannotChangeStakeException, InvalidActionException, BlackjackGamePlayerAction as GameAction
import pickle

import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType


class Bot:
    def __init__(self, access_token, group, game_data_path='data.pickle'):
        self.access_token = access_token
        self.group_id = group

        self.launched = datetime.now()
        self.logger = logging.getLogger('app.bot')

        self.game_data_path = game_data_path

        try:
            with open(game_data_path, "rb") as f:
                self.games = pickle.load(f)
        except OSError:
            self.logger.warning("Game data not found, starting blank...")
            self.games = dict()

        self.logger.info("Initializing new Bot (group: {}, token: {}...)".format(self.group_id, self.access_token[:3]))

        self.session = vk_api.VkApi(token=self.access_token)
        self.vk = self.session.get_api()
        self.uploader = vk_api.VkUpload(self.vk)
        self.longpoll = VkBotLongPoll(self.session, self.group_id, wait=5)

        self.logger.info("VK API: success")

    def event_loop(self):
        for event in self.longpoll.listen():
            if event.type == VkBotEventType.MESSAGE_NEW:
                self.handle_message(event.object)
            else:
                self.handle_event(event)

    def status(self):
        return "Blackjack-Bot is serving {} players.".format(len(self.games))

    def save_game_data(self):
        try:
            with open(self.game_data_path, "wb") as f:
                pickle.dump(self.games, f)
            self.logger.info("Game data saved to {}".format(self.game_data_path))
        except OSError:
            self.logger.error("Failed to save game data to {}".format(self.game_data_path))

    def shutdown_jobs(self):
        self.save_game_data()


    game_actions_map = {
        '/start': GameAction.START,
        '/go': GameAction.START,
        'го': GameAction.START,
        '/hit': GameAction.HIT,
        '/card': GameAction.HIT,
        '/take': GameAction.HIT,
        'карту': GameAction.HIT,
        'карта': GameAction.HIT,
        '/stand': GameAction.STAND,
        '/pass': GameAction.STAND,
        'хватит': GameAction.STAND,
        'хорош': GameAction.STAND,
        'всё': GameAction.STAND,
        'все': GameAction.STAND
    }

    game_actions_reverse_map = {
        GameAction.START: '/start - начать игру',
        GameAction.HIT: '/card - взять ещё одну карту',
        GameAction.STAND: '/pass - отказаться брать карты'
    }

    def handle_message(self, message):
        self.logger.info("{:>10}: {}".format(message.from_id, repr(message.text)))

        if message.from_id not in self.games.keys():
            self.games[message.from_id] = BlackjackGame()

        game = self.games[message.from_id]

        if len(message.text) > 0:
            args = message.text.lower().split()

            if args[0] == '/status':
                response = self.status()
                self.save_game_data()
            elif args[0] == '/me':
                response = "Ваш баланс: {}🍃\nВсего партий: {}\nВыиграно: {} ({:.2%})".format(game.balance, game.total_games, game.stats['won'], game.winrate)
                self.save_game_data()
            elif args[0] == '/stake':
                response = ""
                try:
                    value = int(args[1])
                    game.set_stake(value)
                    response = "Ставка изменена."
                except ValueError:
                    response = "Неверное значение \"{}\" (должно быть от {} до {})".format(args[1], *game.stake_limits)
                except (KeyError, IndexError):
                    response = "Использование: /stake число (от {} до {})".format(*game.stake_limits)
                except CannotChangeStakeException:
                    response = "Нельзя изменить свою ставку, когда карты розданы!"
            elif args[0] in self.game_actions_map.keys():
                try:
                    response = game.advance(self.game_actions_map[args[0]])
                except InvalidActionException as e:
                    response = "Вы сейчас можете: \n{}".format(
                        '\n'.join(self.game_actions_reverse_map[action] for action in e.allowed_actions)
                    )
            elif args[0] == '/top':
                top_amount = min(7, len(self.games))
                top = sorted(
                    self.games.items(), key=lambda g: -g[1].balance
                )[:top_amount]

                response = "Топ-{top_players} игроков (из {total_players}):\n\n{top}".format(
                    top_players=top_amount,
                    total_players=len(self.games),
                    top="\n".join(
                        "{}: {}🍃 -- @id{}".format(item[0]+1, item[1][1].balance, item[1][0]) for item in enumerate(top)
                    )
                )
            else:
                response = "/stake сумма -- установить размер ставки\n/start -- начать новую партию\n/take -- взять ещё карту\n/pass -- перестать брать карты\n/top -- топ лучших игроков\n/me -- ваша статистика"
            if response:
                self.respond(message, response)

    def respond(self, message, text, attach=None):
        self.vk.messages.send(
            message=text,
            random_id=randint(1, 1000000),
            peer_id=message.peer_id,
            attachment=attach or None
        )

    def handle_event(self, event):
        pass
