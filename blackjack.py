from enum import Enum
from random import choice, shuffle


class CardSuit(Enum):
    Diamonds = 1
    Hearts = 2
    Clubs = 3
    Spades = 4


class CardValue(Enum):
    Ace = 1
    Two = 2
    Three = 3
    Four = 4
    Five = 5
    Six = 6
    Seven = 7
    Eight = 8
    Nine = 9
    Ten = 10
    Jack = 11
    Queen = 12
    King = 13


class Card:
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit

    signs = {
        CardSuit.Diamonds: '♦',
        CardSuit.Hearts: '♥',
        CardSuit.Clubs: '♣',
        CardSuit.Spades: '♠',
        CardValue.Ace: 'A',
        CardValue.Two: '2',
        CardValue.Three: '3',
        CardValue.Four: '4',
        CardValue.Five: '5',
        CardValue.Six: '6',
        CardValue.Seven: '7',
        CardValue.Eight: '8',
        CardValue.Nine: '9',
        CardValue.Ten: '10',
        CardValue.Jack: 'J',
        CardValue.Queen: 'Q',
        CardValue.King: 'K',
    }

    emojiful = {
        CardSuit.Diamonds: '♦️',
        CardSuit.Hearts: '♥️',
        CardSuit.Clubs: '♣️',
        CardSuit.Spades: '♠️',
    }

    def __str__(self):
        return "[{}{}]".format(Card.signs[self.value], Card.signs[self.suit])

    def nice(self, show_suit=True, use_emoji=True):
        result = Card.signs[self.value]
        if show_suit:
            if use_emoji:
                result += Card.emojiful[self.suit]
            else:
                result += Card.signs[self.suit]
        return result

    def score(self, soft=False):
        if self.value in [CardValue.Jack, CardValue.Queen, CardValue.King]:
            return 10
        elif self.value in [CardValue.Ace]:
            return 1 if soft else 11
        else:
            return self.value.value  # enum value

    @staticmethod
    def create_deck(decks=1):
        if decks > 8:
            raise ValueError("Too many decks, fella!")
        deck = list()
        for suit in CardSuit:
            for value in CardValue:
                for i in range(decks):
                    deck.append(Card(value=value, suit=suit))
        shuffle(deck)
        return deck


class BlackjackGameState(Enum):
    JUST_STARTED = 0
    CARDS_DEALT = 1
    WON = 100
    LOST = 101


class BlackjackGamePlayerAction(Enum):
    START = 10
    HIT = 1
    STAND = 0


class BlackjackException(Exception):
    pass


class CannotChangeStakeException(BlackjackException):
    pass


class InvalidActionException(BlackjackException):
    def __init__(self, **kwargs):
        super().__init__()
        self.allowed_actions = kwargs['allowed_actions']


class BlackjackGame:
    def __init__(self):
        self.balance = 0
        self.state = None
        self.stake = 4
        self.stake_limits = (1, 100)
        self.advance()
        self.dealer_hand = []
        self.player_hand = []
        self.recent_message = None
        self.stats = {
            'won': 0,
            'lost': 0,
            'ties': 0,
        }

    rules = {
        'soft_ace': True,
        'blackjack_reward': 1.499999
    }

    deck = Card.create_deck(1)

    def set_stake(self, value):
        if self.state is not BlackjackGameState.CARDS_DEALT:
            if self.stake_limits[0] <= value <= self.stake_limits[1]:
                self.stake = value
            else:
                raise ValueError
        else:
            raise CannotChangeStakeException

    @property
    def total_games(self):
        return self.stats['won'] + self.stats['lost'] + self.stats['ties']

    @property
    def winrate(self):
        return self.stats['won']/self.total_games if self.total_games > 0 else 0

    def deal(self, amount):
        if len(self.deck) >= amount:
            return (choice(self.deck) for _ in range(amount))
        else:
            raise ValueError("Not enough cards in a deck!")

    def score_hand(self, hand):
        sum = 0
        cards = 0
        for card in hand:
            cards += 1
            soft = self.rules['soft_ace'] and (sum + card.score() > 21) and (cards <= 2)
            sum += card.score(soft=soft)
        return sum

    def reset(self):
        self.dealer_hand = []
        self.player_hand = []

    def end(self, coef):
        if coef > 0:
            self.state = BlackjackGameState.WON
            delta = int(round(self.stake * coef))
            self.balance += delta
            self.stats['won'] += 1
            return " (+{}🍃)".format(delta)
        elif coef < 0:
            self.state = BlackjackGameState.LOST
            delta = int(round(self.stake * coef))
            self.balance += delta
            self.stats['lost'] += 1
            return " (−{}🍃)".format(-delta)
        else:
            self.stats['ties'] += 1
            self.state = BlackjackGameState.LOST
            return ""

    @property
    def text_summary(self):
        return "Рука дилера: {} == {}\n\nВаша рука: {} == {}\n".format(
            ' '.join(card.nice() for card in self.dealer_hand),
            self.score_hand(self.dealer_hand),
            ' '.join(card.nice() for card in self.player_hand),
            self.score_hand(self.player_hand)
        )

    def deal_cards(self, player_action):
        self.dealer_hand.extend(self.deal(1))
        self.player_hand.extend(self.deal(2))
        message = self.text_summary

        if self.score_hand(self.player_hand) == 21:
            message += "\nBlackjack!"
            if self.score_hand(self.dealer_hand) >= 10:
                new_card = next(self.deal(1))
                self.dealer_hand.append(new_card)
                message += "\nУ дилера есть туз. Он раздаёт себе вторую карту ({}), ".format(new_card.nice())
                if self.score_hand(self.dealer_hand) == 21:
                    message += "набирая 21 очко. Вы проиграли партию." + self.end(-1)
                else:
                    message += "но победа остаётся за вами." + self.end(self.rules['blackjack_reward'])
            else:
                message += " Эта партия за вами." + self.end(self.rules['blackjack_reward'])
        else:
            self.state = BlackjackGameState.CARDS_DEALT

        if self.score_hand(self.player_hand) > 21:
            raise RuntimeWarning("Player can't insta-lose!")

        if self.score_hand(self.dealer_hand) > 21:
            raise RuntimeWarning("Player can't insta-lose!")

        return message

    def hit_or_stand(self, player_action):
        if player_action is BlackjackGamePlayerAction.HIT:
            new_card = next(self.deal(1))
            self.player_hand.append(new_card)

            message = "Вы взяли карту ({})\n\n".format(new_card.nice())

            player_score = self.score_hand(self.player_hand)

            if player_score > 21:
                message += self.text_summary + "\nВы проиграли партию." + self.end(-1)
                return message
            elif player_score == 21:
                return message + self.finale()
            else:
                return self.text_summary + message

        elif player_action is BlackjackGamePlayerAction.STAND:
            return self.finale()

        else:
            raise InvalidActionException(allowed_actions=[BlackjackGamePlayerAction.HIT, BlackjackGamePlayerAction.STAND])

    def finale(self):
        # player decided to stop; it is time for dealer to take cards
        message = self.text_summary

        new_cards = []
        while self.score_hand(self.dealer_hand) < 17:
            new_card = next(self.deal(1))
            new_cards.append(new_card)
            self.dealer_hand.append(new_card)

        if len(new_cards) > 1:
            message += "\nДилер раздаёт себе карты: {},".format(
                ' '.join(card.nice() for card in new_cards)
            )
        elif len(new_cards) == 1:
            message += "\nДилер берёт карту {},".format(new_cards[0].nice())

        dealer_score = self.score_hand(self.dealer_hand)
        player_score = self.score_hand(self.player_hand)
        if dealer_score > 21:
            message += " проигрывая вам партию с перебором.".format(dealer_score) + self.end(1)
        elif dealer_score == 21 and player_score < 21:
            message += " набирая 21 очко и выигрывая партию." + self.end(-1)
        elif player_score == 21:
            message += " но не набирает 21 очко и эта партия остаётся за вами." + self.end(self.rules['blackjack_reward'])
        elif dealer_score > player_score:
            message += " набирая {} очков против ваших {}.".format(dealer_score, player_score) + self.end(-1)
        elif dealer_score == player_score:
            message += " набирая {} очков и заканчивая партию вничью.".format(dealer_score) + self.end(0)
        elif dealer_score < player_score:
            message += " набирая только {} очков. Победа за вами!".format(dealer_score) + self.end(1)
        else:
            message += " а колода-то в киоске заряжена!" + self.end(100)

        return message

    def advance(self, player_action=None):
        if self.state is None:
            self.state = BlackjackGameState.JUST_STARTED
        elif self.state in [BlackjackGameState.JUST_STARTED, BlackjackGameState.WON, BlackjackGameState.LOST]:
            self.reset()
            return self.deal_cards(player_action)
        elif self.state in [BlackjackGameState.CARDS_DEALT]:
            return self.hit_or_stand(player_action)
        else:
            return "Какая-то хуйня непонятная получилась."


if __name__ == "__main__":
    game = BlackjackGame()

    cmd = None

    cmd_bind = {
        'hit': BlackjackGamePlayerAction.HIT,
        'stand': BlackjackGamePlayerAction.STAND
    }

    while True:
        print(game.advance(cmd))
        cmd = input()
        cmd = cmd_bind.get(cmd, None)
