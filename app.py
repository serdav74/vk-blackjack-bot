import json
import logging
import sys
from time import sleep

from requests.exceptions import Timeout, ConnectionError

from bot import Bot


def load_config():
    config_filename = sys.argv[1] if len(sys.argv) >= 2 else 'config.json'
    with open(config_filename, 'rt') as f:
        return json.load(f)


def setup_logging(level):
    logger = logging.getLogger('app')
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(fmt="{asctime:>16} | {levelname:8} | {message}", style='{')

    handler.setFormatter(formatter)
    handler.setLevel(level)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger


def serve():
    reconnect_delay = 5

    config = load_config()
    logger = setup_logging(config["logging_level"])
    bot = Bot(access_token=config['access_token'], group=config['group_id'])
    while True:
        try:
            bot.event_loop()
        except KeyboardInterrupt:
            logger.info("Keyboard interrupt. Shutting down...")
            break
        except Timeout:
            logger.warning("Timeout. Reconnecting...")
            pass
        except ConnectionError:
            logger.warning("Connection error. Reconnecting in {} seconds...".format(reconnect_delay))
            sleep(reconnect_delay)
            reconnect_delay = (reconnect_delay - 5) % 20 + 6
            pass
        finally:
            bot.save_game_data()
    bot.shutdown_jobs()


if __name__ == '__main__':
    serve()
